
public class RepeatItus {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//Q1: The total number of characters in the sentence - COMMIT YOUR CODE
//Q2: The last 15 characters in the string - COMMIT YOUR CODE
//Q3: The number of times �c� and �p� appear in the sentence - COMMIT YOUR CODE
//		You must write LOGIC to do this
//		You may NOT do a static output, eg: System.out.println(�Number of c & p = 15�)

		String sentence = "copyandpastecopyandpastecopyandpastecopyandpastecopyandpastecopyandpastecommitandpushcommitandpushcommitandpushcommitandpushpushcccccommmitttsss";

//		Q1
		int charNumber = sentence.length();
		System.out.println("The total number of characters in the sentence is: " + charNumber);

//		Q2 
		String lastFifteen = sentence.substring(sentence.length() - 15);
		System.out.println("The last 15 characters in the string is : " + lastFifteen);

//		Q3

		int timesC = 0;
		int timesP = 0;

		char[] charArray = sentence.toCharArray();
//		Q3 - C
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == 'c') {
				timesC++;
			}
		}
		System.out.println("The number of times �c� appear in the sentence is: " + timesC);
//		Q3 - P
		for (int i = 0; i < charArray.length; i++) {
			if (charArray[i] == 'p') {
				timesP++;
			}
		}
		System.out.println("The number of times �p� appear in the sentence is: " + timesP);
	}
}
